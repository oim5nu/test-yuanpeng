const faker = require("faker");
const loki = require("lokijs");
const db = new loki("Officeworks");

const generateCollection = () => {
  return new Promise((resolve, reject) => {
    try {
      let availability = db.addCollection("availability", { indices: ["id"] });

      for (let i = 0; i < 10; i++) {
        availability.insert({
          id: i.toString(),
          name: faker.commerce.productName(),
          description: faker.company.catchPhrase(),
          weight: faker.random.number({
            min: 10,
            max: 50
          }),
          available: faker.random.boolean(),
          postcode: "3000"
        });
      }
      resolve(availability);
    } catch(error) {
      reject(error);
    }
  });
};

const getByPostcode = async (postcode, itemCode) => {
  let foundItems = [];
  try {
    let availability = await generateCollection();
    for (let i=0; i < itemCode.length; i++) {
      const item = itemCode[i];
      const foundItem = availability.find({ id: item, postcode })[0];
      foundItems.push(foundItem);
    }
    return foundItems;
  } catch(error) {
    throw error;
  }
}; 

module.exports = {
  getByPostcode
};
