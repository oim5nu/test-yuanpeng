const loki = require("lokijs");
const db = new loki("Officeworks");
const { isEmpty } = require('../library/utils');

const generateCollection = () => {
  return new Promise((resolve, reject) => {
    try {
      let deliveryCost = db.addCollection("delivery");

      for (var i = 1; i < 5; i++) {
        deliveryCost.insert({
          weight: i * 10,
          cost: i * 2,
          postcode: "3000"
        });
      }
      resolve(deliveryCost);
    } catch(error) {
      reject(error);
    }
  });
};

const getByWeight =  (postcode, wt) => {
  return new Promise(async (resolve, reject) => {
    const defaultCost = {
      weight: 0,
      cost: 0,
      postcode: ''
    };
    try {
      
      const deliveryCost = await generateCollection();
      let cost = deliveryCost.find({ weight: { $gt: wt } }, postcode)[0];
      if (isEmpty(cost)) {
        cost = { ...defaultCost };
      }
      resolve(cost);
    } catch(error) {
      reject(error);
    }
  });
};

module.exports = {
  getByWeight
};
