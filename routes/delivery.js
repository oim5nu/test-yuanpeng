const router = require("express").Router(),
  availabilityService = require("../services/availability"),
  logger = require("../services/logger");
const { isEmpty } = require('../library/utils');

const ERRS = {
  "Invalid Part": 404,
  "Invalid Store": 404,
  "Invalid Params": 400
};

function respondError(res, desc) {
  const code = ERRS[desc];
  return res.status(code == undefined ? 500 : code).send({ errors: [desc] });
}

// Good to past error to error middleware for further handling
const getService = async (req, res, /*next*/) => {
  try {
    let { params: { postcode }, query: { partNumber } } = req;

    if (isEmpty(partNumber)) {
      const error = "invalid part number specified";
      throw error;
    }
    // postcode, partNumber suggested to be wrapped in an object  
    const data = await availabilityService.getByPostcode(postcode, partNumber);
    return res.json(data);
  } catch(err) {
    logger.error(err);
    return respondError(res, err);
  }
};

router.get("/:postcode", getService);

module.exports = router;
