const supertest = require('supertest');
const chai = require('chai');
const should = chai.should();

// This agent refers to PORT where program is running.
const server = supertest.agent("http://localhost:3000");

describe("# /api/delivery", function() {
  it("should return items and cost", async () => {
    const response = await server.get("/api/delivery/3000")
      .query("partNumber=1,3,5")
      .expect(200);     
    
    response.status.should.equal(200);
    response.body.should.be.an('array');      
  });
});


