const app = require("express")();
const bodyParser = require("body-parser");
const winston = require("express-winston");
const logger = require("./services/logger");
const apiDelivery = require(`./routes/delivery`);
const config = require("./config")();

// Good to go into basic error handling middleware
process.on("uncaughtException", function(error) {
  // It is suggested to provide process error handling mechanism here rather than 
  // output the error to the console only, for example email associated members
  console.dir(error);
  console.log("uncaughtException");

  if (error.stack) console.log(error.stack);
});

app.use(bodyParser.json());
// suggested to go into middleware customer library
const winstonLogger = winston.logger({
  winstonInstance: logger,
  meta: false,
  msg: "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}",
  expressFormat: true,
  colorize: true,
  ignoreRoute: () => { return false; }
});
app.use(winstonLogger);
app.use("/api/delivery", apiDelivery);

const { port } = config;

app.listen(port);

console.log("Server listening on port: " + port);
