// Suggested to provide a logger or log folder to have more customer settings
// like lever of log, where to persist the log etc, which depends on the needs
const winston = require("winston");
winston.addColors({
  info: "green",
  warn: "cyan",
  error: "red",
  verbose: "blue",
  i: "gray",
  db: "magenta"
});
const logger = new winston.Logger({
  transports: [
    new winston.transports.Console({ colorize: false, timestamp: true })
  ]
});

module.exports = logger;
