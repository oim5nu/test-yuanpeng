const deliverService = require("../services/delivery");

const getByPostcode = async (postcode, partCode) => {
  let result = [];
  try {
    const data = await deliverService.getByPostcode(postcode, partCode);
    result.push(data);
  } catch(error) {
    throw error;
  }
  return result;
};

module.exports = {
  getByPostcode
};
