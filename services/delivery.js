const availabilityModel = require("../model/Availability");
const deliveryModel = require("../model/Delivery");
const { isEmpty } = require('../library/utils');

const getByPostcode = async (postcode, items) => {
  let final = {
    items: [],
    cost: 0
  };

  try {
    let checkItems = items.split(",");
    let availability = await availabilityModel.getByPostcode(postcode, checkItems);
    let finalItems = [];
    let finalCost = 0;
    for (let i=0; i < availability.length; i++) {
      let av = { ...availability[i] };
      const { available } = av;
      if ( available === true) {
        let defaultCost = 0;
        finalItems.push(av);
        const { postcode, weight } = av;
        const delCosts = await deliveryModel.getByWeight(postcode, weight);
        if (delCosts) {
          let { cost } = delCosts;
          finalCost += isEmpty(cost) ? defaultCost : Number(cost);
        }        
      }
    }
    final = { items: [...finalItems], cost: finalCost };
    return final;
  } catch (error) {
    throw error;
  }
};

module.exports = {
  getByPostcode
};
